#ifndef PERSONREPOS_H
#define PERSONREPOS_H

#include <list>
#include "person.h"
#include <fstream>
#include <iostream>
#include <QTsql>
#include <QString>

class personrepos
{
public:
    personrepos();
    QSqlDatabase db;
    void addperson(person p);
    void datainput();
    void showlist();
    //void datainputstart();
    //bool is_empty(ifstream &inputfile);
    std::list<person> list();
    void clearlist();
    std::list<person> searchpick(string input, string searchstring);
    std::list<person> searchpick(string input, int year);
    std::list<person> sort(string sortby);
private:
    std::list<person> personlist;
};

#endif // PERSONREPOS_H



