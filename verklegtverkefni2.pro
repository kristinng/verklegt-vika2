#-------------------------------------------------
#
# Project created by QtCreator 2014-11-30T11:10:16
#
#-------------------------------------------------

QT       += core sql

QT       -= gui

TARGET = verklegtverkefni
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    consoleui.cpp \
    personservice.cpp \
    person.cpp \
    personrepos.cpp \
    computerservice.cpp \
    computer.cpp \
    computerrepos.cpp \
    linkerservice.cpp \
    linkerrepos.cpp \
    utilities.cpp

HEADERS += \
    consoleui.h \
    personservice.h \
    person.h \
    personrepos.h \
    computerservice.h \
    computer.h \
    computerrepos.h \
    linkerservice.h \
    linkerrepos.h \
    utilities.h
