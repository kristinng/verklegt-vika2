#include "computerrepos.h"

computerrepos::computerrepos()
{
    computerlist = std::list<computer>();

    QString connectionName = "dataConnection";
    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName("database.sqlite");
    }
}

void computerrepos::addcomputer(computer c){//this function simply takes in a computer and pushes it into the list and then

    db.open();
    QSqlQuery query(db);
    query.prepare("INSERT INTO computer VALUES (NULL, :name, :type, :built, :builtyear)");
    query.bindValue(":name", QString::fromStdString(c.name));
    query.bindValue(":type", QString::fromStdString(c.type));
    query.bindValue(":built",c.built);
    query.bindValue(":builtyear", c.builtyear);
    query.exec();
    db.close();

}

void computerrepos::datainput(){//this function opens a input text file that the user can put into the list at any time
    ifstream inputfile("inputtext.txt");//so we can offer the user a simple way to fill his list
    char temp;//it simply opens the text file and reads the name until the first tab character
    int check=0;//after that if the program confirms we added a name then it can assume we have a person being added
    computerrepos per;//and is allowed to add the rest of the information.
    if(inputfile.is_open()){
        while(!inputfile.eof())
        {
            bool anothercomputer=false;
            check=0;
            computer c = computer();
                do{
                       inputfile.get(temp);
                       if((temp != '\t')&(temp != '\n')){
                           c.name += temp;
                           anothercomputer=true;
                       }
                       else{
                           check++;
                       }
                  }while(check==0);
              if(anothercomputer==true){
                    inputfile >> c.type >> c.built >> c.builtyear;
                    addcomputer(c);
                    }
        }
    inputfile.close();
    }
}

list<computer> computerrepos::list(){
    db.open();
    std::list<computer>SQList;
    QSqlQuery query(db);
    query.exec("SELECT Name, Type, Built, Builtyear FROM computer;");
    while(query.next()){
        computer c = computer();
        c.name = query.value("Name").toString().toStdString();
        c.type = query.value("Type").toString().toStdString();
        c.built = query.value("Built").toInt(); //toBool???
        c.builtyear = query.value("Builtyear").toInt();
        SQList.push_back(c);
    }
    db.close();
    return SQList;
}
list<computer> computerrepos::searchpick(string input,string searchstring){//search gets an input so it knows if its checking gender,name,birth or death  and a searchstring/year

    db.open();
    std::list<computer>SQList;
    QSqlQuery query(db);
    if(input == "name"){
        query.prepare("SELECT Name, Type, Built, Builtyear FROM computer WHERE Name LIKE (:name)");
        query.bindValue(":name", QString::fromStdString("%"+searchstring+"%"));
        query.exec();
        while(query.next()){
            computer c = computer();
            c.name = query.value("Name").toString().toStdString();
            c.type = query.value("Type").toString().toStdString();
            c.built = query.value("Built").toInt();
            c.builtyear = query.value("Builtyear").toInt();
            SQList.push_back(c);
        }
    }
    else if(input == "type"){
        query.prepare("SELECT Name, Type, Built, Builtyear FROM computer WHERE Type LIKE (:type)");
        query.bindValue(":type", QString::fromStdString(searchstring+"%"));
        query.exec();
        while(query.next()){
            computer c = computer();
            c.name = query.value("Name").toString().toStdString();
            c.type = query.value("Type").toString().toStdString();
            c.built = query.value("Built").toInt();
            c.builtyear = query.value("Builtyear").toInt();
            SQList.push_back(c);
        }
    }

    db.close();
    return SQList;
}
list<computer> computerrepos::searchpick(string input,int year){

    db.open();
    std::list<computer>SQList;
    QSqlQuery query(db);
    if(input == "built"){
        query.prepare("SELECT Name, Type, Built, Builtyear FROM computer WHERE Built LIKE (:built)");
        query.bindValue(":built", year); // á að vera ,year hérna?
        query.exec();
        while(query.next()){
            computer c = computer();
            c.name = query.value("Name").toString().toStdString();
            c.type = query.value("Type").toString().toStdString();
            c.built = query.value("Built").toInt();
            c.builtyear = query.value("Builtyear").toInt();
            SQList.push_back(c);
        }
    }
    else if(input == "year"){
        query.prepare("SELECT Name, Type, Built, Builtyear FROM computer WHERE Builtyear LIKE (:builtyear)");
        query.bindValue(":builtyear", year);
        query.exec();
        while(query.next()){
            computer c = computer();
            c.name = query.value("Name").toString().toStdString();
            c.type = query.value("Type").toString().toStdString();
            c.built = query.value("Built").toInt();
            c.builtyear = query.value("Builtyear").toInt();
            SQList.push_back(c);
        }
    }

    db.close();
    return SQList;
}
list<computer> computerrepos::sort(string sortby){
    std::list<computer>sortedlist;
    computer c = computer();
    db.open();
    QSqlQuery query(db);

    if (sortby == "name"){

        query.exec("SELECT Name, Type, Built, Builtyear FROM computer ORDER BY Name");
    }
    else if (sortby == "type"){
        query.exec("SELECT Name, Type, Built, Builtyear FROM computer ORDER BY Type");
    }
    else if (sortby == "built"){
        query.exec("SELECT Name, Type, Built, Builtyear FROM computer ORDER BY Built DESC");
    }
    else if (sortby == "builtyear"){
        query.exec("SELECT Name, Type, Built, Builtyear FROM computer ORDER BY Builtyear");
    }
    else if (sortby == "rname"){
        query.exec("SELECT Name, Type, Built, Builtyear FROM computer ORDER BY Name DESC");
    }
    else if (sortby == "rtype"){
        query.exec("SELECT Name, Type, Built, Builtyear FROM computer ORDER BY Type DESC");
    }
    else if (sortby == "rbuilt"){
        query.exec("SELECT Name, Type, Built, Builtyear FROM computer ORDER BY Built ");
    }
    else if (sortby == "rbuiltyear"){
        query.exec("SELECT Name, Type, Built, Builtyear FROM computer ORDER BY Builtyear DESC");
    }
    while(query.next()){
        computer c = computer();
        c.name = query.value("Name").toString().toStdString();
        c.type = query.value("Type").toString().toStdString();
        c.built = query.value("Built").toInt();
        c.builtyear = query.value("Builtyear").toInt();
        sortedlist.push_back(c);
    }
    db.close();
    return sortedlist;
}
