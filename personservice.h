#ifndef PERSONSERVICE_H
#define PERSONSERVICE_H

#include "person.h"
#include "personrepos.h"
#include <list>
#include <algorithm>

class personservice
{
public:
    personservice();
    void addperson(person p);
    void datainput();
    void showlist();
    //void datainputstart();
    std::list<person> list();
    void clearlist();
    std::list<person> searchpick(string input, string searchstring);
    std::list<person> searchpick(string input, int year);
    std::list<person> sort(string sortby);
private:
    personrepos personrep;
};

#endif // PERSONSERVICE_H
