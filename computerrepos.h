#ifndef COMPUTERREPOS_H
#define COMPUTERREPOS_H

#include <list>
#include "computer.h"
#include <fstream>
#include <iostream>
#include <QTsql>
#include <QString>

class computerrepos
{
public:
    computerrepos();
    QSqlDatabase db;
    void addcomputer(computer c);
    void datainput();
    void showlist();
    std::list<computer> list();
    std::list<computer> sort(string sortby);
    std::list<computer> searchpick(string input, string searchstring);
    std::list<computer> searchpick(string input, int year);

private:  std::list<computer> computerlist;
};

#endif // COMPUTERREPOS_H
