#include "linkerservice.h"

linkerservice::linkerservice()
{
    Linkerrepos=linkerrepos();
}

list<computer> linkerservice::personconnections(string connectionname){

    return Linkerrepos.personconnections(connectionname);
}
list<person> linkerservice::computerconnections(string connectionname){

    return Linkerrepos.computerconnections(connectionname);
}
void linkerservice::personaddconnection(person p, string computername){
    Linkerrepos.personaddconnection(p,computername);
}
void linkerservice::computeraddconnection(computer c, string personname){
    Linkerrepos.computeraddconnection(c,personname);
}
