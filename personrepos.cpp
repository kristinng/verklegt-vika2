#include "personrepos.h"
/*
 * we check to see if we have a connection
 * if we dont we connect it
 * else we refer to it
 */
personrepos::personrepos()
{
    personlist = std::list<person>();

    QString connectionName = "dataConnection";
    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName("database.sqlite");     
    }
}
/*
 * simplly adding a person p to the list using bindvalue
 */
void personrepos::addperson(person p){
    db.open();
    QSqlQuery query(db);
    query.prepare("INSERT INTO person VALUES (NULL, :name, :gender, :birthyear, :deathyear)");
    query.bindValue(":name", QString::fromStdString(p.name));
    query.bindValue(":gender", QString::fromStdString(p.gender));
    query.bindValue(":birthyear", p.birthyear);
    query.bindValue(":deathyear", p.deathyear);
    query.exec();
    db.close();
}
/*this function opens a input text file that
 *the user can put into the list at any time
 *opens the text file and reads the name until the first tab character
 */
void personrepos::datainput(){
    ifstream inputfile("inputtext.txt");
    char temp;
    int check=0;
    personrepos per;
    if(inputfile.is_open()){
        while(!inputfile.eof())
        {
            bool anotherperson=false;
            check=0;
            person p = person();
                do{
                       inputfile.get(temp);
                       if((temp != '\t')&(temp != '\n')){
                           p.name += temp;
                           anotherperson=true;
                       }
                       else{
                           check++;
                       }
                  }while(check==0);
              if(anotherperson==true){
                    inputfile >> p.gender >> p.birthyear >> p.deathyear;
                    addperson(p);
                    }
        }
    inputfile.close();
    }
}
/*
 * we return a list of all people
 */
list<person> personrepos::list(){
    db.open();
    std::list<person>SQList;
    QSqlQuery query(db);
    query.exec("SELECT Name, Gender, Birthyear, Deathyear FROM person;");
    while(query.next()){
        person p = person();
        p.name = query.value("Name").toString().toStdString();
        p.gender = query.value("Gender").toString().toStdString();
        p.birthyear = query.value("Birthyear").toInt();
        p.deathyear = query.value("Deathyear").toInt();
        SQList.push_back(p);
    }
    db.close();
    return SQList;
}
/*
 *search gets an input so it knows if its checking gender,name,birth or death  and a searchstring/year
 * we query for anything that resembles the searchstring with wildcards
 * and we return a list og search results
 */
list<person> personrepos::searchpick(string input,string searchstring){

    db.open();
    std::list<person>SQList;
    QSqlQuery query(db);
    if(input == "name"){
        query.prepare("SELECT Name, Gender, Birthyear, Deathyear FROM person WHERE Name LIKE (:name)");
        query.bindValue(":name", QString::fromStdString("%"+searchstring+"%"));
        query.exec();
        while(query.next()){
            person p = person();
            p.name = query.value("Name").toString().toStdString();
            p.gender = query.value("Gender").toString().toStdString();
            p.birthyear = query.value("Birthyear").toInt();
            p.deathyear = query.value("Deathyear").toInt();
            SQList.push_back(p);
        }
    }
    else if(input == "gender"){
        query.prepare("SELECT Name, Gender, Birthyear, Deathyear FROM person WHERE Gender LIKE (:gender)");
        query.bindValue(":gender", QString::fromStdString(searchstring+"%"));
        query.exec();
        while(query.next()){
            person p = person();
            p.name = query.value("Name").toString().toStdString();
            p.gender = query.value("Gender").toString().toStdString();
            p.birthyear = query.value("Birthyear").toInt();
            p.deathyear = query.value("Deathyear").toInt();
            SQList.push_back(p);
        }
    }
    db.close();
    return SQList;
}
/*
 *See descritpion of searchpick above
 * only difference is this one searches for years
 */
list<person> personrepos::searchpick(string input,int year){

    db.open();
    std::list<person>SQList;
    QSqlQuery query(db);
    if(input == "birthyear"){
        query.prepare("SELECT Name, Gender, Birthyear, Deathyear FROM person WHERE Birthyear LIKE (:birthyear)");
        query.bindValue(":birthyear", year);
        query.exec();
        while(query.next()){
            person p = person();
            p.name = query.value("Name").toString().toStdString();
            p.gender = query.value("Gender").toString().toStdString();
            p.birthyear = query.value("Birthyear").toInt();
            p.deathyear = query.value("Deathyear").toInt();
            SQList.push_back(p);
        }
    }
    else if(input == "deathyear"){
        query.prepare("SELECT Name, Gender, Birthyear, Deathyear FROM person WHERE Deathyear LIKE (:deathyear)");
        query.bindValue(":deathyear", year);
        query.exec();
        while(query.next()){
            person p = person();
            p.name = query.value("Name").toString().toStdString();
            p.gender = query.value("Gender").toString().toStdString();
            p.birthyear = query.value("Birthyear").toInt();
            p.deathyear = query.value("Deathyear").toInt();
            SQList.push_back(p);
        }
    }

    db.close();
    return SQList;
}
/*
 * just sorting from sql database and returning a list of the sorted database
 * sql makes sorting very easy so this this be self explanotory
 */
list<person> personrepos::sort(string sortby){
    std::list<person>sortedlist;
    person p = person();
    db.open();
    QSqlQuery query(db);
    if (sortby == "name"){
        query.exec("SELECT Name, Gender, Birthyear, Deathyear FROM person ORDER BY Name");
    }
    else if (sortby == "gender"){
        query.exec("SELECT Name, Gender, Birthyear, Deathyear FROM person ORDER BY Gender");
    }
    else if (sortby == "birth"){
        query.exec("SELECT Name, Gender, Birthyear, Deathyear FROM person ORDER BY Birthyear");
    }
    else if (sortby == "death"){
        query.exec("SELECT Name, Gender, Birthyear, Deathyear FROM person ORDER BY Deathyear");
    }
    else if (sortby == "rname"){
        query.exec("SELECT Name, Gender, Birthyear, Deathyear FROM person ORDER BY Name DESC");
    }
    else if (sortby == "rgender"){
        query.exec("SELECT Name, Gender, Birthyear, Deathyear FROM person ORDER BY Gender DESC");
    }
    else if (sortby == "rbirth"){
        query.exec("SELECT Name, Gender, Birthyear, Deathyear FROM person ORDER BY Birthyear DESC");
    }
    else if (sortby == "rdeath"){
        query.exec("SELECT Name, Gender, Birthyear, Deathyear FROM person ORDER BY Deathyear DESC");
    }
    while(query.next()){
        person p = person();
        p.name = query.value("Name").toString().toStdString();
        p.gender = query.value("Gender").toString().toStdString();
        p.birthyear = query.value("Birthyear").toInt();
        p.deathyear = query.value("Deathyear").toInt();
        sortedlist.push_back(p);
    }
    db.close();
    return sortedlist;
}

