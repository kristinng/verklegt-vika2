#include "consoleui.h"


using namespace std;

ConsoleUI::ConsoleUI()
{
    Personservice = personservice();
    Computerservice = computerservice();
    Linkerservice = linkerservice();
    Utilities = utilities();
}

void ConsoleUI::start()
{

    string input;
    bool exit=false;
    char charinput;
    while(exit==false){
        clear();
        cout << "This program can make a list of computer scientists and sort them as you want.\n";
        system("color 7");
        cout << "These are all the possible commands:\n";
        cout << "\"add\" - This command adds a person or computer to the list.\n";
        cout << "\"show\" - This command displays the a list on the screen.\n";
        cout << "\"search\" - This command searches a list.\n";
        cout << "\"exit\" - This command exits the program.\n";
        cin >> input;
        if(input == "add"){
            clear();
            add();
        }
        else if(input == "show"){
            clear();
            bool exit1 = false;
            while(exit1 == false){
                clear();
                system("color 7");
                cout << "The possibilities are:\n";
                cout << "*List of persons\n";
                cout << "*List of computers\n";
                cout << "*List of connections between persons and computers\n";
                cout << "Type \"p\" for a list of persons, \"c\" for a list of computers and\n";
                cout << "\"n\" for a list of connections.\n";
                cin >> charinput;

                if(tolower(charinput)=='p'){
                    clear();
                    exit1 = true;
                    personsortoptions();
                }
                else if(tolower(charinput)=='c'){
                    clear();
                    exit1 = true;
                    computersortoptions();
                }
                else if(tolower(charinput)== 'n'){
                    clear();
                    exit1 = true;
                    connectionoptions();
                }
                else {
                    clear();
                    cout << "This is not a valid option!\n";
                    cout << endl;
                }
            }
        }
        else if(input == "search"){
            clear();
            string again;
            bool exit=false;
            while(exit==false){
                searchlist();
                cout << "Type \"yes\" if you would like to search again. Anything else exits.\n";
                cin >> again;
                if(again !="yes"){
                    exit=true;
                }
            }
        }
        else if(input == "exit"){
            exit = true;
        }
        else{
            clear();
            cout << "That is not a valid command.\n\n";
        }
    }
}
void ConsoleUI::showlist(list<person>plist){
    //clear();
    cout << "_______________________________________________________________________\n\n";
    for(list<person>::iterator iter = plist.begin(); iter != plist.end(); iter++){
        system("color D");
        cout << std::left<<setw(30) << iter->name << setw(25) << iter->gender << setw(12) << iter->birthyear << setw(12) << iter->deathyear<< "\n";
    }
    cout << "_______________________________________________________________________\n\n";

}
void ConsoleUI::showlistcomputer(list<computer>clist){
    //clear();
    cout << "________________________________________________________________________\n\n";
    for(list<computer>::iterator iter = clist.begin(); iter != clist.end(); iter++){
        system("color D");
        cout  << std::left<<setw(30) << iter->name << setw(25) << iter->type << setw(12) << iter->built << setw(12) << iter->builtyear<< "\n";
    }
    cout << "________________________________________________________________________\n\n";
}

void ConsoleUI::searchlist(){
    clear();
    system("color 7");
    string input,searchstring;
    char opt;
    int year;
    list<person>resultslist;
    list<computer>computerlist;
    cout << "Do you want to search by a person or by a computer?\n";
    cout << "Input \"p\" for person and \"c\" for computer.\n";
    cin >> opt;
    if(tolower(opt) == 'p'){
        clear();
        cout << "These are all possible search options: \n";
        cout << "Type \"name\" - to search by name.\n";
        cout << "Type \"gender\" - to search by gender.\n";
        cout << "Type \"birthyear\" - to search by birthyear.\n";
        cout << "Type \"deathyear\" - to search by deathyear.\n";
        cin >> input;
        cin.ignore();
        if(input == "name"){
            clear();
            do{
                cout << "Input a name to search for: ";
                getline (cin,searchstring);
                resultslist = Personservice.searchpick(input, searchstring);
            }while(Utilities.contains_number(searchstring));
            if(0<resultslist.size()){
                cout << resultslist.size()<<" results found: \n";
                showlist(resultslist);
            }
            else{
                cout << "No results found.\n\n";
            }
        }
        else if(input == "gender"){
            clear();
            do{
                cout << "Input gender you would like to search for: ";
                cin >> searchstring;
                resultslist=Personservice.searchpick(input, searchstring);
            }while(Utilities.contains_number(searchstring));
            if(0<resultslist.size()){//here we check if the list has any elements, if it does not we have not found any matches
                cout << resultslist.size()<<" results found: \n";
                showlist(resultslist);
            }
            else{
                cout << "No results found.\n\n";
            }
        }
        else if(input == "birthyear"){
            clear();
            string temp;
            do{
                cout << "Input a year to search for: ";
                cin >> temp;
            }while(!Utilities.is_number(temp));
            year = std::atoi(temp.c_str());
            resultslist=Personservice.searchpick(input, year);
            if(0<resultslist.size()){
                cout << resultslist.size()<<" results found: \n";
                showlist(resultslist);
            }
            else{
                cout << "No results found.\n\n";
            }
        }
        else if(input == "deathyear"){
            clear();
            string temp;
            do{
                cout << "Input a year to search for: ";
                cin >> temp;
            }while(!Utilities.is_number(temp));
            year = std::atoi(temp.c_str());
            resultslist=Personservice.searchpick(input, year);
            if(0<resultslist.size()){
                cout << resultslist.size()<<" results found: \n";
                showlist(resultslist);
            }
            else{
                cout << "No results found.\n\n";
            }
        }
        else{
            cout << "That is not a valid option\n\n";
        }
    }
    else if(tolower(opt) == 'c'){
        cout << "These are all possible search options: \n";
        cout << "Type \"name\" - to search by name.\n";
        cout << "Type \"type\" - to search by type.\n";
        cout << "Type \"built\" - to search by the computer was build or not.\n";                  // athuga!!
        cout << "Type \"year\" - to search by year it was built.\n";
        cin >> input;
        cin.ignore();
        if(input == "name"){
            clear();
            cout << "Input a name to search for: ";
            do{
                getline (cin,searchstring);
                computerlist = Computerservice.searchpick(input, searchstring);
            }while(Utilities.contains_number(searchstring));
            if(0<computerlist.size()){
                cout << computerlist.size()<<" results found: \n";
                showlistcomputer(computerlist);
            }
            else{
                cout << "No results found.\n\n";
            }
        }
        else if(input == "type"){
            clear();
            do{
                cout << "Input type you would like to search for: ";
                cin >> searchstring;
                computerlist=Computerservice.searchpick(input, searchstring);
            }while(Utilities.contains_number(searchstring));
            if(0<computerlist.size()){//here we check if the list has any elements, if it does not we have not found any matches
                cout << computerlist.size()<<" results found: \n";
                showlistcomputer(computerlist);
            }
            else{
                cout << "No results found.\n\n";
            }
        }
        else if(input == "built"){
            clear();
            do{
                cout << "Type \"1\" for all built computers or \"0\" for those not built";
                cin >> year;
                computerlist=Computerservice.searchpick(input, year);//footnote year is used for bool for parameter purpose
            }while(year !=1  && year !=0 && cout << "Invalid input, try again \n");
            if(0<computerlist.size()){
                cout << computerlist.size()<<" results found: \n";
                showlistcomputer(computerlist);
            }
            else{
                cout << "No results found.\n\n";
            }
        }
        else if(input == "year"){
            clear();
            string temp;
            do{
                cout << "Input a year to search for: ";
                cin >> temp;
            }while(Utilities.is_number(temp));
            year = std::atoi(temp.c_str());
            computerlist=Computerservice.searchpick(input, year);
            if(0<computerlist.size()){
                cout << computerlist.size()<<" results found: \n";
                showlistcomputer(computerlist);
            }
            else{
                cout << "No results found.\n\n";
            }
        }
        else{
            cout << "That is not a valid option\n\n";
        }
    }
}
void ConsoleUI::personsortoptions(){
    bool exit=false;
    string sortby, again;
    while(exit==false){
        clear();
        system("color 7");
        cout << "Type \"list\" to see the list as it is.\n";
        cout << "List can be sorted by \"name\", \"gender\", \"birth\" or \"death\".\n";
        cout << "Type an \"r\" in front of any of the sort options available to see them reversed, for example \"rname\".\n";
        cout << "Type \"exit\" to return to the main menu.\n";
        cin >> sortby;
        if(sortby == "list"){
            list<person>plist;
            plist=Personservice.list();
            showlist(plist);
            cout << "If you would like to try a different sort type \"again\". Anything else exits.\n";
            cin >> again;
            if(again != "again"){
                exit=true;
            }
        }
        else if(sortby != "exit"){
            showlist(Personservice.sort(sortby));
            cout << "If you would like to try a different sort type \"again\". Anything else exits.\n";
            cin >> again;
            if(again != "again"){
                exit=true;
            }
        }
        else if(sortby == "exit"){
            exit=true;
        }
        else{
            cout << "Invalid input, try again.\n";
        }
    }
}
void ConsoleUI::computersortoptions(){
    bool exit=false;
    string again, sortby;
    while(exit==false){
        cout << "Type \"list\" to see the original list as it is.\n";
        cout << "List can be sorted by \"name\", \"type\", \"built\" or \"builtyear\".\n";
        cout << "Type an \"r\" in front of any of the sort options available to see them reversed, for example \"rname\".\n";
        cout << "Type \"exit\" to return to the main menu.\n";
        cin >> sortby;
        if(sortby == "list"){
            list<computer>clist;
            clist=Computerservice.list();
            showlistcomputer(clist);
            cout << "If you would like to try a different sort type \"again\". Anything else exits\n";
            cin >> again;
            if(again != "again"){
                exit=true;
            }
        }
        else if(sortby != "exit"){
            showlistcomputer(Computerservice.sort(sortby));
            cout << "If you would like to try a different sort type \"again\". Anything else exits\n";
            cin >> again;
            if(again != "again"){
                exit=true;
            }
        }
        else if(sortby == "exit"){
            exit=true;
        }
        else{
            cout << "Invalid input, try again.\n";
        }
    }
}
void ConsoleUI::add(){
    char input;
    cout << "Would you like to add a person or a computer to the list?\n";
    cout << "Type \"p\" for person and \"c\" for computer.\n";
    cin >> input;
    if(tolower(input) == 'p' ){
        clear();
        cout << "Type \"m\" for manual input or \"t\" for text file input:\n";
        cin >> input;
        if(tolower(input) =='t'){
            Personservice.datainput();

            cout << "Data from text file has been added to the list.\n\n";
        }
        else if(tolower(input) == 'm'){
            clear();
            string again;
            bool exit=false;
            string temp,computername;
            char again2;
            while(exit==false){
                person p = person();
                cin.ignore();
                do{
                    cout << "Input a name: ";
                    getline(cin, p.name);
                }while(Utilities.contains_number(p.name));
                do{
                    cout << "Input gender: ";
                    cin >> p.gender;
                }while(Utilities.contains_number(p.gender));
                do{
                    cout << "Input year of birth: ";
                    cin >> temp;
                }while(!Utilities.is_number(temp));
                p.birthyear = std::atoi(temp.c_str());
                do{
                    cout << "Input year of death: ";
                    cin >>temp;
                }while(!Utilities.is_number(temp) || (p.birthyear>std::atoi(temp.c_str()) && cout<<"Invalid input, try again\n"));
                p.deathyear= std::atoi(temp.c_str());
                Personservice.addperson(p);
                cout << "Do you want to connect this person to an inventions?\n";
                cout << "type \"y\" for yes, anything else exits\n";
                cin >> again2;
                if(tolower(again2)=='y'){
                    clear();
                    list<computer>clist;
                    clist=Computerservice.list();
                    showlistcomputer(clist);
                    cout << "These are all the people in our database\n";
                    cout << "Enter the name of the person you want to connect to your computer:\n";
                    cin >> computername;
                    Linkerservice.personaddconnection(p, computername);
                }
                cout << "Person successfully added!\n";
                cout << "Type \"yes\" if you would like to add another person. Anything else exits.\n";
                cin >> again;
                clear();
                if(again !="yes"){
                    clear();
                    exit=true;
                }
            }
        }

    }
    else if(tolower(input) == 'c')
    {
        string choice;
        string again, personname;
        string temp;
        char again2;
        bool exit=false;
        bool exit2=false;
        while(exit==false){
            computer c = computer();
            cin.ignore();
            do{
            cout << "Input a name: ";
            getline(cin, c.name);
            }while(Utilities.contains_number(c.name));
            do{
            cout << "Input type: ";
            getline(cin, c.type);
            }while(Utilities.contains_number(c.type));
            cout << "Was the computer built? Input \"yes\" or \"no\". ";
            cin >> choice;
            while(exit2==false){
                if(choice == "yes" ||choice == "Yes"){
                    c.built=1;
                    do{
                    cout << "What year was the computer built? ";
                    cin >> temp;
                    }while(!Utilities.is_number(temp));
                    c.builtyear = std::atoi(temp.c_str());
                    exit2=true;
                }
                else if(choice == "no" || choice == "No"){
                    c.built=0;
                    c.builtyear = -1;
                    exit2=true;
                }
                else {
                    cout << "Invalid input. Try again!";
                }
            }
            Computerservice.addcomputer(c);
            cout << "Do you want to connect this person to an inventions?\n";
            cout << "type \"y\" for yes, anything else exits\n";
            cin >> again2;
            if(tolower(again2)=='y'){
                clear();
                list<person>plist;
                plist=Personservice.list();
                showlist(plist);
                cout << "These are all the computers in our database\n";
                cout << "Enter the name of the computer you want to connect to your person:\n";
                cin >> personname;
                Linkerservice.computeraddconnection(c, personname);

            }
            cout << "Computer successfully added!\n";
            cout << "Type \"yes\" if you would like to add another computer. Anything else exits.\n";
            cin >> again;
            if(again !="yes"){
                exit=true;
            }
        }

    }
    else {
        cout << "Invalid input.";
    }
}
void ConsoleUI::clear() {
    cout << string( 100, '\n' );
}
void ConsoleUI::connectionoptions(){
    bool exit=false;
    list<person>plist;
    list<computer>clist;
    string input;
    string name="name";
    clear();
    do{
        cout << "Input \"person\" to search for a person and see what computers he/she worked on\n";
        cout << "or input \"computer\" to search for a computer and see what person worked on it\n";
        cin >> input;
        if(input=="person"){
            cout << "Type the Inventors ";
            cin.ignore();
            getline(cin, input);
            plist = Personservice.searchpick(name,input);
            if(plist.size()<1){
                cout << " No matching name found. \n";
            }
            else if(1<plist.size()){
                cout <<"Too many results found, please be more specific";
            }
            else if(plist.size()==1){
                cout <<"You have chosen to see the inventions associated with: \n";
                showlist(plist);
                cout << "These are inventions he was a part of: \n";
                clist = Linkerservice.personconnections(input);
                showlistcomputer(clist);
            }
        }
        else if(input == "computer"){
            cout << "Type atleast a piece of the name of the computer: ";
            cin.ignore();
            getline(cin, input);
            clist = Computerservice.searchpick(name,input);
            if(clist.size()<1){
                cout << " No matching name found. \n";
            }
            else if(1<clist.size()){
                cout <<"Too many results found, please be more specific";
            }
            else if(clist.size()==1){
                cout <<"You have chosen to see people associated with: \n";
                showlistcomputer(clist);
                cout << "These are the people that were a part of this computer: \n";
                plist = Linkerservice.computerconnections(input);
                showlist(plist);
            }
        }
        else{
            cout << "invalid option";
        }
        cout << "Type \"yes\" if you want to search again. Anything else exits";
        cin >> input;
        if(input != "yes"){
            exit=true;
        }
    }while(exit==false);
}
