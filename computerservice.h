#ifndef COMPUTERSERVICE_H
#define COMPUTERSERVICE_H

#include "computer.h"
#include "computerrepos.h"
#include <list>
#include <algorithm>

class computerservice
{
public:
    computerservice();
    computerrepos Computerrep;
    void addcomputer(computer c);
    void datainput();
    void showlist();
    std::list<computer> list();
    std::list<computer> sort(string sortby);
    std::list<computer> searchpick(string input, string searchstring);
    std::list<computer> searchpick(string input, int year);
private:
    computerrepos computerrep;
};

#endif // COMPUTERSERVICE_H
