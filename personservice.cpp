#include "personservice.h"

personservice::personservice()
{
    personrep = personrepos();
}

void personservice::addperson(person p)
{
    personrep.addperson(p);
}
void personservice::datainput(){
    personrep.datainput();
}
list<person> personservice::list(){
    return personrep.list();
}
list<person> personservice::searchpick(string input,string searchstring){
    return personrep.searchpick(input, searchstring);
}
list<person> personservice::searchpick(string input,int year){
    return personrep.searchpick(input, year);
}
list<person> personservice::sort(string sortby){

    return personrep.sort(sortby);
}

