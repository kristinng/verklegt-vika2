#include "linkerrepos.h"

using namespace std;

linkerrepos::linkerrepos()
{
    QString connectionName = "dataConnection";
    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName("database.sqlite");
    }
}
/*
 * We find all the links and then
 * the links where name equals our search name
 * we return their values to a person and return a list of those persons
 */
list<computer> linkerrepos::personconnections(string connectionname){
    list<computer>SQList;
    db.open();
    QSqlQuery query(db);
    query.prepare("SELECT C.Name, C.Type, C.Built, C.Builtyear "
                  "FROM person P, linker L, computer C "
                  "WHERE P.PersonID = L.P_ID AND C.ComputerID = L.C_ID AND P.Name LIKE (:name)");
    query.bindValue(":name", QString::fromStdString("%"+connectionname+"%"));
    query.exec();
    while(query.next()){
        computer c = computer();
        c.name = query.value("Name").toString().toStdString();
        c.type = query.value("Type").toString().toStdString();
        c.built = query.value("Built").toInt();
        c.builtyear = query.value("Builtyear").toInt();
        SQList.push_back(c);
    }
    db.close();
    return SQList;
}
/*
 * same as above but with computers
 */
list<person> linkerrepos::computerconnections(string connectionname){
    list<person>SQList;
    db.open();
    QSqlQuery query(db);
    query.prepare("SELECT P.Name, P.Gender, P.Birthyear, P.Deathyear "
                  "FROM person P, linker L, computer C "
                  "WHERE P.PersonID = L.P_ID AND C.ComputerID = L.C_ID AND C.Name LIKE (:name)");
    query.bindValue(":name", QString::fromStdString("%"+connectionname+"%"));
    query.exec();
    while(query.next()){
        person p = person();
        p.name = query.value("Name").toString().toStdString();
        p.gender = query.value("Gender").toString().toStdString();
        p.birthyear= query.value("Birthyear").toInt();
        p.deathyear = query.value("Deathyear").toInt();
        SQList.push_back(p);
    }
    db.close();
    return SQList;
}
/*
 * we find the ID's of the computer and person we want to connect
 * then we connect them by inserting their id's into the linker table
 */
void linkerrepos::personaddconnection(person p,string computername){

    int temp=0, temp2=0;

    db.open();

    QSqlQuery query(db);
    query.prepare("SELECT P.PersonID, C.ComputerID "
                  "FROM person P, computer C "
                  "WHERE P.Name LIKE (:pname) AND C.Name LIKE (:cname)");
    query.bindValue(":pname", QString::fromStdString("%"+p.name+"%"));
    query.bindValue(":cname", QString::fromStdString("%"+computername+"%"));
    query.exec();
    while(query.next()){
        temp = query.value("PersonID").toInt();
        temp2 = query.value("ComputerID").toInt();
    }


    QSqlQuery query2(db);
    query.prepare("INSERT INTO linker VALUES (:cid, :pid)");
    query.bindValue(":cid",temp);
    query.bindValue(":pid", temp2);
    query.exec();

    db.close();
}
/*
 * same function as above but reversed
 */
void linkerrepos::computeraddconnection(computer c,string personname){

    int temp=0, temp2=0;

    db.open();
    QSqlQuery query(db);
    query.prepare("SELECT P.PersonID, C.ComputerID "
                  "FROM person P, computer C "
                  "WHERE P.Name LIKE (:pname) AND C.Name LIKE (:cname)");
    query.bindValue(":pname", QString::fromStdString("%"+personname+"%"));
    query.bindValue(":cname", QString::fromStdString("%"+c.name+"%"));
    query.exec();
    while(query.next()){
        temp = query.value("PersonID").toInt();
        temp2 = query.value("ComputerID").toInt();
    }
    QSqlQuery query2(db);
    query.prepare("INSERT INTO linker VALUES (:cid, :pid)");
    query.bindValue(":cid",temp);
    query.bindValue(":pid", temp2);
    query.exec();

    db.close();
}
