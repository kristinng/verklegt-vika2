#ifndef LINKERREPOS_H
#define LINKERREPOS_H
#include <QTsql>
#include <iostream>
#include <string>
#include <list>
#include "person.h"
#include "computer.h"


class linkerrepos
{
public:
    linkerrepos();
    QSqlDatabase db;
    list<computer> personconnections(string connectionname);
    list<person> computerconnections(string connectionname);
    void personaddconnection(person p, string computername);
    void computeraddconnection(computer c, string personname);
};

#endif // LINKERREPOS_H
