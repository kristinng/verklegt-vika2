#include "computerservice.h"

computerservice::computerservice()
{
    Computerrep=computerrepos();
}
void computerservice::addcomputer(computer c)
{
    Computerrep.addcomputer(c);
}
void computerservice::datainput(){
    Computerrep.datainput();
}

list<computer> computerservice::list(){
    return Computerrep.list();
}

list<computer> computerservice::sort(string sortby){

    return computerrep.sort(sortby);
}
list<computer> computerservice::searchpick(string input,string searchstring){
    return computerrep.searchpick(input, searchstring);
}
list<computer> computerservice::searchpick(string input,int year){
    return computerrep.searchpick(input, year);
}
