#ifndef CONSOLEUI_H
#define CONSOLEUI_H
#include <iostream>
#include <string>
#include <stdio.h>
#include <cctype>
#include <stdlib.h>
#include <iomanip>
#include "personservice.h"
#include "computerservice.h"
#include "linkerservice.h"
#include "utilities.h"

class ConsoleUI
{
public:
    ConsoleUI();
    void start();
    void showlist(list<person>plist);
    void searchlist();
    void personsortoptions();
    void add();
    void computersortoptions();
    void showlistcomputer(list<computer> clist);
    void connectionoptions();
    void clear();
private:
    personservice Personservice;
    computerservice Computerservice;
    linkerservice Linkerservice;
    utilities Utilities;
};

#endif // CONSOLEUI_H
