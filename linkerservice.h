#ifndef LINKERSERVICE_H
#define LINKERSERVICE_H
#include <linkerrepos.h>
#include <list>
#include <string>
#include <algorithm>
using namespace std;
class linkerservice
{
public:
    linkerservice();

    linkerrepos Linkerrepos;
    list<computer> personconnections(string connectionname);
    list<person> computerconnections(string connectionname);   
    void personaddconnection(person p, string computername);
    void computeraddconnection(computer c, string personname);
};

#endif // LINKERSERVICE_H
